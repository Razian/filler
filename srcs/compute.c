/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compute.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 03:26:16 by tchivert          #+#    #+#             */
/*   Updated: 2019/11/15 17:44:09 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		ft_checkpiece(t_filler *env, t_vec *pos, size_t count)
{
	size_t	i;

	i = 0;
	env->p_piece.y = -1;
	while (++env->p_piece.y < env->s_piece.y)
	{
		env->p_piece.x = -1;
		while (++env->p_piece.x < env->s_piece.x)
		{
			if (env->piece[env->p_piece.y][env->p_piece.x] == '*')
			{
				i++;
				if (i == count)
				{
					pos->y = env->p_piece.y;
					pos->x = env->p_piece.x;
					return (1);
				}
			}
		}
	}
	return (0);
}

int		ft_testpiece(t_filler *env, int rety, int retx)
{
	int		collide;
	size_t	count;
	t_vec	pos;

	collide = 0;
	count = 0;
	while (ft_checkpiece(env, &pos, ++count))
	{
		if (rety + pos.y >= 0 && retx + pos.x >= 0 &&
			rety + pos.y < env->s_map.y &&
			retx + pos.x < env->s_map.x)
		{
			if (env->map[rety + pos.y][retx + pos.x] != '.')
				collide++;
		}
		else
		{
			env->place.y = 0;
			env->place.x = 0;
			return (0);
		}
		if (collide > 1)
			return (0);
	}
	return (1);
}

int		ft_checkhere(t_filler *env)
{
	size_t	count;
	t_vec	pos;

	count = 0;
	pos.y = 0;
	pos.x = 0;
	ft_checkpiece(env, &pos, ++count);
	while (!(ft_testpiece(env, env->place.y - pos.y, env->place.x - pos.x)))
		if (!(ft_checkpiece(env, &pos, ++count)))
			return (0);
	env->place.y = env->place.y - pos.y;
	env->place.x = env->place.x - pos.x;
	return (1);
}

void	ft_compute_x(t_filler *env)
{
	env->p_map.y = -1;
	while (++env->p_map.y < env->s_map.y)
	{
		env->p_map.x = -1;
		while (++env->p_map.x < env->s_map.x)
		{
			if (env->map[env->p_map.y][env->p_map.x] == env->player ||
				env->map[env->p_map.y][env->p_map.x] == env->player + 32)
			{
				env->place.y = env->p_map.y;
				env->place.x = env->p_map.x;
				if (ft_checkhere(env))
					return ;
			}
		}
	}
}

void	ft_compute_o(t_filler *env)
{
	env->p_map.y = env->s_map.y;
	while (--env->p_map.y > 0)
	{
		env->p_map.x = env->s_map.x;
		while (--env->p_map.x > 0)
		{
			if (env->map[env->p_map.y][env->p_map.x] == env->player ||
				env->map[env->p_map.y][env->p_map.x] == env->player + 32)
			{
				env->place.y = env->p_map.y;
				env->place.x = env->p_map.x;
				if (ft_checkhere(env))
					return ;
			}
		}
	}
}
