/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/15 15:32:50 by tchivert          #+#    #+#             */
/*   Updated: 2019/11/15 18:06:46 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	ft_freetab(char **tab, int size)
{
	int	i;

	i = -1;
	while (tab[++i] && i < size)
		ft_strdel(&tab[i]);
	free(tab);
}

int		main(void)
{
	char		*line;
	void		(*compute)(t_filler*);
	t_filler	env;

	while (get_next_line(0, &line))
	{
		if ((env.err = ft_parse(line, &env)) && env.err == 1)
		{
			compute = (env.player == 'O' ? &ft_compute_o : &ft_compute_x);
			(*compute)(&env);
			ft_printt("%d %d\n", env.place.y, env.place.x);
			ft_freetab(env.map, env.s_map.y);
			ft_freetab(env.piece, env.s_piece.y);
		}
		if (env.err == -1)
		{
			ft_freetab(env.map, env.s_map.y);
			ft_freetab(env.piece, env.s_piece.y);
			break ;
		}
	}
	return (0);
}
