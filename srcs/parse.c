/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/15 15:46:22 by tchivert          #+#    #+#             */
/*   Updated: 2019/11/15 18:05:31 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static int	ft_errcheck(char **tab, t_vec sizes)
{
	t_vec	i;

	i.x = 0;
	i.y = -1;
	while (++i.y < sizes.y)
	{
		if (!tab[i.y])
			return (-1);
		i.x = -1;
		while (++i.x < sizes.x)
		{
			if (!tab[i.y][i.x] || !ft_strchr(".xXoO*", tab[i.y][i.x]))
				return (-1);
		}
	}
	return (0);
}

static char	**ft_parse_map(char *line, t_filler *env)
{
	int		i;
	char	**map;
	char	**size;

	i = -1;
	size = ft_strsplit(line, ' ');
	env->s_map.y = ft_atoi(size[1]);
	env->s_map.x = ft_atoi(size[2]);
	if (!(map = (char **)malloc(sizeof(char *) * env->s_map.y)))
		return (NULL);
	ft_strdel(&line);
	get_next_line(0, &line);
	ft_strdel(&line);
	while (++i < env->s_map.y)
	{
		if (!get_next_line(0, &line))
		{
			ft_strdel(&line);
			break ;
		}
		map[i] = ft_strdup(&line[4]);
		ft_strdel(&line);
	}
	ft_freetab(size, 3);
	env->err = ft_errcheck(map, env->s_map);
	return (map);
}

static char	**ft_parse_piece(char *line, t_filler *env)
{
	int		i;
	char	**piece;
	char	**size;

	i = -1;
	size = ft_strsplit(line, ' ');
	env->s_piece.y = ft_atoi(size[1]);
	env->s_piece.x = ft_atoi(size[2]);
	if (!(piece = (char **)malloc(sizeof(char *) * env->s_piece.y)))
		return (NULL);
	ft_strdel(&line);
	while (++i < env->s_piece.y)
	{
		if (!get_next_line(0, &line))
		{
			ft_strdel(&line);
			break ;
		}
		piece[i] = ft_strdup(line);
		ft_strdel(&line);
	}
	ft_freetab(size, 3);
	env->err = ft_errcheck(piece, env->s_piece);
	return (piece);
}

int			ft_parse(char *line, t_filler *env)
{
	if (!ft_strncmp(line, "$$$ exec p", 10))
	{
		env->player = line[10] == '1' ? 'O' : 'X';
		ft_strdel(&line);
		return ((env->player == 'O' || env->player == 'X') ? 0 : -1);
	}
	else if (!ft_strncmp(line, "Plateau", 7))
	{
		env->map = ft_parse_map(line, env);
		return (env->err == -1 ? -1 : 0);
	}
	else if (!ft_strncmp(line, "Piece", 5))
	{
		env->piece = ft_parse_piece(line, env);
		return (env->err == -1 ? -1 : 1);
	}
	ft_strdel(&line);
	return (0);
}
