/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tchivert <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 02:34:00 by tchivert          #+#    #+#             */
/*   Updated: 2019/11/15 17:54:58 by tchivert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

# include <stdlib.h>
# include "../libft/libft.h"

typedef struct	s_vec
{
	int			x;
	int			y;
}				t_vec;

typedef struct	s_filler
{
	int			err;
	char		player;
	char		**map;
	char		**piece;
	t_vec		p_map;
	t_vec		p_piece;
	t_vec		s_map;
	t_vec		s_piece;
	t_vec		place;
}				t_filler;

int				ft_parse(char *line, t_filler *env);
void			ft_compute_o(t_filler *env);
void			ft_compute_x(t_filler *env);
void			ft_freetab(char **tab, int size);

#endif
