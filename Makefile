# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tchivert <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/09/11 04:07:29 by tchivert          #+#    #+#              #
#    Updated: 2019/11/15 20:50:36 by tchivert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = tchivert.filler
SRC = srcs/filler.c \
	  srcs/parse.c \
	  srcs/compute.c
LIBFT = libft/libft.a
INCLUDES = ./includes
CC = clang -Werror -Wall -Wextra


all: $(NAME)

$(NAME): $(LIBFT) $(SRC)
	@printf "filler:%80s\r" "[---                 ]"
	@sleep 0.1
	@printf "filler:%80s\r" "[--------            ]"
	@$(CC) $(SRC) $(LIBFT) -I$(INCLUDES) -o $(NAME)
	@printf "filler:%80s\r" "[--------------      ]"
	@sleep 0.1
	@printf "filler:%80s\n" "[--------------------]"
	@echo "$(NAME) successfully created"

$(LIBFT):
	@printf "libft:%81s\r" "[---                 ]"
	@sleep 0.1
	@make -C libft fclean
	@printf "libft:%81s\r" "[--------            ]"
	@sleep 0.1
	@make -C libft
	@printf "libft:%81s\r" "[-------------       ]"
	@sleep 0.1
	@printf "libft:%81s\n" "[--------------------]"

clean:
	@make -C libft clean
	@printf "clean:%81s\n" "[--------------------]"

fclean: clean
	@make -C libft fclean
	@printf "fclean:%80s\n" "[--------------------]"
	@/bin/rm -f $(NAME)

re: fclean all
